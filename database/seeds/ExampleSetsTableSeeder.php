<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ExampleSetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('example_sets')->delete();
        $faker = Faker::create('en_US');

        foreach(range(1,20) as $index){
            App\ExampleSet::create([
                'title' => $faker->catchphrase,
                'markdown' => "#Some fancy set of examples",
                'username' => App\User::all()->random()->name,
                'upvotes' => rand(-10, 400),
                'created_at' => $faker->dateTimeThisYear,
                'updated_at' => $faker->dateTimeThisYear
            ]);
        }
    }
}
