<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class FavoritesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('favorites')->delete();
        $faker = Faker::create('en_US');

        foreach(range(1,200) as $index){
            App\Favorite::create([
                'example_id' => App\Example::all()->random()->id,
                'username' => App\User::all()->random()->name,
                'created_at' => $faker->dateTimeThisYear,
                'updated_at' => $faker->dateTimeThisYear
            ]);
        }
    }
}
