@extends('layouts.app')

@section('head')
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row" v-for="example in examples">
                <example :example="example"></example>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
@endsection
@section('foot')
    <!-- JavaScripts -->
<script src="{{ URL::to('js/main.js') }}"></script>
@endsection
